import time
from PIL import Image
from pyglet.gl import *
import numpy as np
from pyglet.window import key

from simulation import Simulation

zoom = 1
res = 7

window = pyglet.window.Window(1000, 800)
keyboard = pyglet.window.key.KeyStateHandler()
window.push_handlers(keyboard)

glEnableClientState(GL_VERTEX_ARRAY)
glEnableClientState(GL_TEXTURE_COORD_ARRAY);


def loadTexture(filename):
	img = Image.open(filename).transpose(Image.FLIP_TOP_BOTTOM)
	textureIDs = (pyglet.gl.GLuint * 1)()
	glGenTextures(1, textureIDs)
	textureID = textureIDs[0]
	glBindTexture(GL_TEXTURE_2D, textureID)
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, img.size[0], img.size[1], 0, GL_RGB, GL_UNSIGNED_BYTE, img.tobytes())
	glBindTexture(GL_TEXTURE_2D, 0)
	return textureID

texture = loadTexture('texture.jpg')


class VBO:
	def __init__(self, data, mode=GL_STATIC_DRAW):
		self.data_gl = (GLfloat * len(data))(*data)
		self.buffer = (GLuint)(0)
		glGenBuffers(1, self.buffer)
		glBindBuffer(GL_ARRAY_BUFFER, self.buffer)
		glBufferData(GL_ARRAY_BUFFER, len(data) * 4, self.data_gl, mode)

	def vertex(self):
		glBindBuffer(GL_ARRAY_BUFFER, self.buffer)
		glVertexPointer(3, GL_FLOAT, 0, 0)

	def texture(self):
		glBindBuffer(GL_ARRAY_BUFFER, self.buffer)
		glTexCoordPointer(2, GL_FLOAT, 0, 0)


class IndexVBO:
	def __init__(self, data, elements):
		self.elements = elements
		self.data_gl = (GLuint * len(data))(*data)
		self.buffer = (GLuint)(0)
		glGenBuffers(1, self.buffer)
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.buffer)
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, len(data) * 4, self.data_gl, GL_STATIC_DRAW)

	def bind(self):
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, self.buffer)

	def __len__(self):
		return self.elements


@window.event
def on_draw():
	glMatrixMode(gl.GL_PROJECTION)
	glLoadIdentity()
	gluPerspective(60, window.width / window.height, 0.1, 100)
	glMatrixMode(gl.GL_MODELVIEW)
	glLoadIdentity()
	glTranslatef(-1, 0, -3.5)

	glClearColor(0, 0.3, 0.5, 0)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)


	for s in [simulation, simulation2]:
		vertices_vbo = VBO(np.reshape(s.vertex, (s.vertex_count * 3, 1)))
		vertices_vbo.vertex()

		glEnable(GL_TEXTURE_2D)
		glBindTexture(GL_TEXTURE_2D, texture)
		tex_coords_vbo.texture()

		glEnable(GL_DEPTH_TEST)
		triangles_vbo.bind()
		glDrawElements(GL_TRIANGLES, len(triangles_vbo), GL_UNSIGNED_INT, 0)

		if wireframe:
			if not wireframe_depth:
				glDisable(GL_DEPTH_TEST)
			else:
				glTranslatef(0, 0, 0.001)
			glDisable(GL_TEXTURE_2D)
			glColor3f(1, 0, 0)
			lines_vbo.bind()
			glDrawElements(GL_LINES, len(lines_vbo), GL_UNSIGNED_INT, 0)
			glColor3f(1, 1, 1)

		glTranslatef(0, -1.2, 0)


@window.event
def on_resize(width, height):
	glViewport(0, 0, width, height)


@window.event
def on_key_press(symbol, modifier):
	if symbol == key.SPACE:
		global wireframe
		wireframe = not wireframe
	elif symbol == key.LCTRL:
		global wireframe_depth
		wireframe_depth = not wireframe_depth


def update(arg):
	simstart = time.time()
	simulation.update()
	#simulation2.update()
	simend = time.time() + 0.00001
	print('Simulation took %fms (%d fps)' % ((simend - simstart) * 1000, 1 / (simend - simstart)))


if __name__ == '__main__':
	simulation = Simulation(res,
							{'y': 0, 'x': 0},
							{'y': 1, 'x': 0}

							#{'y': 1}
	)

	simulation2 = Simulation(res,
							{'y': 0, 'x': 0},
							{'y': 1, 'x': 0},
							 constrain_with='spring'
							# {'y': 1}
							)

	tex_coords_vbo = VBO(np.reshape(simulation.texcoord, (simulation.vertex_count * 2, )))
	triangles_vbo = IndexVBO(simulation.triangle_list, simulation.triangle_list.shape[0])
	lines_vbo = IndexVBO(simulation.line_list, simulation.line_list.shape[0])

	vertices_vbo = VBO(np.reshape(simulation.vertex, (simulation.vertex_count * 3, 1)))

	wireframe = True
	wireframe_depth = False
	pyglet.clock.schedule_interval(update, 1 / 60.0)
	pyglet.app.run()

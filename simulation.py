# import noise
import numpy as np
import numpy.linalg as li
import scipy.sparse as sp
import scipy.sparse.linalg as spli

matrix = sp.lil_matrix
solve = spli.spsolve
np.set_printoptions(precision=5, threshold=700, edgeitems=10, linewidth=220, suppress=True)


class Constraint:
	def build_S_matrix(self, vertex_count, *indices):
		S = matrix((len(indices) * 3, vertex_count * 3))
		for i, index in enumerate(indices):
			S[i * 3 + 0, index * 3 + 0] = 1
			S[i * 3 + 1, index * 3 + 1] = 1
			S[i * 3 + 2, index * 3 + 2] = 1
		return S

	@property
	def lhs(self):
		return self.stiffness * self.S.T.dot(self.A.T.dot(self.A.dot(self.S)))

	def precompute(self, current_vertices):
		pass


class AttachmentConstraint(Constraint):
	A = sp.identity(3)
	B = A

	def __init__(self, stiffness, index, position, vertex_count):
		self.stiffness = stiffness
		self.index = index
		self.position = position
		self.S = self.build_S_matrix(vertex_count, index)

	def solve(self, current_vertices, other_constraints, rhs):
		rhs[self.index] += self.stiffness * self.position


class SpringConstraint(Constraint):
	A = matrix((6, 6)); A[:] = np.identity(6); A[3:,:3] = -np.identity(3); A[:3, 3:] = -np.identity(3); A *= 0.5
	B = A

	def __init__(self, stiffness, index_i, index_j, rest_distance, vertex_count):
		self.stiffness = stiffness
		self.index_i = index_i
		self.index_j = index_j
		self.rest_distance = rest_distance
		self.S = self.build_S_matrix(vertex_count, index_i, index_j)

	def solve(self, current_vertices, other_constraints, rhs):
		displacement = current_vertices[self.index_i] - current_vertices[self.index_j]
		norm = li.norm(displacement)
		if not norm:
			return
		update = (norm - self.rest_distance) / 2 * (displacement / norm)

		p1 = current_vertices[self.index_i] - update
		p2 = current_vertices[self.index_j] + update
		internal_force = (p1 - p2) * (self.stiffness / 2)
		rhs[self.index_i] += internal_force
		rhs[self.index_j] -= internal_force


class ARAPCellConstraint(Constraint):

	def __init__(self, stiffness, index_i, neighbours, rest_vertex, vertex_count):
		self.stiffness = stiffness
		self.index_i = index_i
		self.neighbours = neighbours
		self.vertex_count = vertex_count

		print(self.index_i)
		print(self.neighbours)

		self.Pi = np.zeros((len(self.neighbours), 3))
		for x, vj_i in enumerate(neighbours):
			print('%d e_%d,%d = %r - %r = %r' % (x, index_i, vj_i, rest_vertex[self.index_i], rest_vertex[vj_i], rest_vertex[self.index_i] - rest_vertex[vj_i]))
			self.Pi[x] = rest_vertex[self.index_i] - rest_vertex[vj_i]

		print(self.Pi)
		# input()

		self.transformation = np.identity(3)
		# self.D = np.identity(len(neighbours))  # weights

	@property
	def lhs(self):
		lhs = matrix((self.vertex_count * 3, self.vertex_count * 3))
		lhs[self.index_i * 3, self.index_i * 3] = len(self.neighbours)
		lhs[self.index_i * 3 + 1, self.index_i * 3 + 1] = len(self.neighbours)
		lhs[self.index_i * 3 + 2, self.index_i * 3 + 2] = len(self.neighbours)
		for neighbour in self.neighbours:
			lhs[self.index_i * 3, neighbour * 3] = -1
			lhs[self.index_i * 3 + 1, neighbour * 3 + 1] = -1
			lhs[self.index_i * 3 + 2, neighbour * 3 + 2] = -1
		return lhs * self.stiffness

	def precompute(self, current_vertices):
		Pi_prime_t = np.zeros((len(self.neighbours), 3))

		# print(len(self.neighbours))
		for x, vj_i in enumerate(self.neighbours):
			Pi_prime_t[x] = current_vertices[self.index_i] - current_vertices[vj_i]

		# S_i = self.Pi.T.dot(Pi_prime_t)

		S_i_2 = self.Pi.T.dot(li.pinv(Pi_prime_t.T))
		S_i = li.pinv(Pi_prime_t).dot(self.Pi)
		print(self.Pi)
		print(Pi_prime_t)
		print(S_i)
		print(S_i_2)
		print()

		S_i = S_i_2
		#input()

		# print(self.Pi)
		# print(Pi_prime_t)
		# print(S_i.dot(Pi_prime_t.T).T)
		# print()

		U, s, V_t = li.svd(S_i)

		print(s)
		#s = np.clip(s, 0.9, 1.1)
		#s[2] = 0
		#print(s)
		# print()
		s = [1, 1, 1]

		self.transformation = V_t.T.dot(np.diag(s).dot(U.T))
		if li.det(self.transformation) < 0:
			# s[2] *= -1
			U[:, 2] *= -1
			self.transformation = V_t.T.dot(np.diag(s).dot(U.T))
			assert li.det(self.transformation) > 0

	def solve(self, current_vertices, other_constraints, rhs):
		for x, neighbour in enumerate(self.neighbours):
			# rhs[self.index_i] += self.Pi[:, x] * self.stiffness
			rhs[self.index_i] += 0.5 * (self.transformation + other_constraints[neighbour].transformation).dot(self.Pi[x]) * self.stiffness


class Simulation:

	def __init__(self, edge_length, *attachment_points, timestep=1, gravity=(0, -0.0098, 0), mass=1, cloth_stiffness=3, attachment_stiffness=50, constrain_with='arap'):
		print('Creating mesh')
		self.vertex_count = edge_length ** 2
		self.vertex = np.zeros((self.vertex_count, 3))
		self.texcoord = np.zeros((self.vertex_count, 2))
		i = 0
		for x in np.linspace(0, 1, edge_length):
			for y in np.linspace(0, 1, edge_length):
				self.vertex[i] = [x, y, 1]
				self.texcoord[i] = [x, y]
				i += 1
		print('Done')

		print('Creating triangle list...')
		self.triangle_list = np.zeros((0, ), dtype=int)
		for i in range((edge_length-1) * edge_length):
			if i % edge_length == edge_length - 1:
				continue
			a, b, c, d = i, i + 1, i + edge_length, i + edge_length + 1
			self.triangle_list = np.hstack((self.triangle_list, [a, b, c], [b, d, c]))
		print('Done')

		print('Creating line list...')
		self.line_list = np.zeros((0,), dtype=int)
		for i in range((edge_length - 1) * edge_length):
			if i % edge_length == edge_length - 1:
				continue
			a, b, c, d = i, i + 1, i + edge_length, i + edge_length + 1
			self.line_list = np.hstack((self.line_list,	[a, b], [b, c], [c, a]))
		for i in range((edge_length-1) * edge_length, edge_length ** 2 - 1):
			self.line_list = np.hstack((self.line_list, [i, i+1]))
		print('Done')

		print('Initialising system...')
		self.velocity = np.zeros(self.vertex.shape)
		self.vertex_mass = mass / self.vertex_count
		self.gravity = gravity
		self.timestep = timestep
		self.constraints = []
		self.time = 0
		print('Done')

		print('Creating cloth constraints...')
		if constrain_with == 'arap':
			rest_vertex = self.vertex.copy()
			for i in range(self.vertex_count):
				has_left = i % edge_length != 0
				has_right = i % edge_length != edge_length - 1
				has_up = i >= edge_length
				has_down = i // edge_length < edge_length - 1
				neighbours = []
				if has_up:
					neighbours.append(i - edge_length)
					if has_left:
						neighbours.append(i - (edge_length + 1))
				if has_down:
					neighbours.append(i + edge_length)
					if has_right:
						neighbours.append(i + edge_length + 1)
				if has_left:
					neighbours.append(i - 1)
				if has_right:
					neighbours.append(i + 1)
				self.constraints.append(ARAPCellConstraint(cloth_stiffness, i, neighbours, rest_vertex, self.vertex_count))
		elif constrain_with == 'spring':
			for i in range(self.vertex_count):
				if i % edge_length == edge_length - 1:
					# right edge
					connected = [i - edge_length]
				elif i < edge_length:
					# top edge
					connected = [i - 1, i + edge_length, i + edge_length + 1]
				else:
					connected = [i + 1, i + edge_length, i + edge_length + 1]
				for j in connected:
					if 0 <= j < edge_length**2:
						self.constraints.append(SpringConstraint(cloth_stiffness, i, j, li.norm(self.vertex[i] - self.vertex[j]), self.vertex_count))
			# special case: top right corner
			self.constraints.append(SpringConstraint(cloth_stiffness, edge_length - 1, edge_length - 2, li.norm(self.vertex[edge_length - 1] - self.vertex[edge_length - 2]), self.vertex_count))
		else:
			print('Cannot constrain mesh with %r' % constrain_with)
		print('Done')

		print('Creating attachments...')
		for i in range(self.vertex_count):
			for attachment_point in attachment_points:
				if 'x' in attachment_point and self.vertex[i, 0] != attachment_point['x']:
					continue
				if 'y' in attachment_point and self.vertex[i, 1] != attachment_point['y']:
					continue
				if 'z' in attachment_point and self.vertex[i, 2] != attachment_point['z']:
					continue
				self.constraints.append(AttachmentConstraint(attachment_stiffness, i, self.vertex[i], self.vertex_count))
		print('Done')

		# LHS of (10)
		print('Computing LHS...')
		self.lhs = (sp.identity(self.vertex_count * 3) * self.vertex_mass) / self.timestep ** 2
		for constraint in self.constraints:
			self.lhs += constraint.lhs
		print('Done')

		print('Creating solver...')
		self.solver = spli.factorized(self.lhs)
		print('Done')

	def update(self, iterations=3):
		# TODO: better wind
		force = np.array(self.gravity)
		#force[0] += (1 + np.sin(self.time * 10)) / 10
		#force[2] -= np.cos(self.time) / 100
		#force *= 0
		# force = np.tile(force, self.vertex_count)

		# line 1 of Algorithm 1
		s = self.vertex + (self.velocity * self.timestep)
		s[:] += (force / self.vertex_mass) * self.timestep ** 2

		q_n_1 = s
		for _ in range(iterations):
			# input()
			for constraint in self.constraints:
				constraint.precompute(q_n_1)

			# RHS of (10)
			rhs = (self.vertex_mass / (self.timestep ** 2)) * s
			for constraint in self.constraints:
				constraint.solve(q_n_1, self.constraints, rhs)

			# solve (10)
			q_n_1 = np.reshape(self.solver(rhs.flatten()), (self.vertex_count, 3))

		self.velocity = (q_n_1 - s) / self.timestep
		self.vertex = q_n_1
		self.time += 0.01

